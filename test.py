import functions
import unittest


class Testfunctions(unittest.TestCase):

    def __init__(self):
        self.float1 = functions.float1
        self.float2 = functions.float2
        self.result = functions.Add()
        self.concatenated = functions.Concatenate
        self.expected = 3
        self.incorrect = 4
        self.type_test = 11.111
        self.hello_there = "Hello there"
        self.general_kenobi = "General Kenobi"

    def test_equal(self):
        self.assertEqual(self.result, self.expected)

    def test_unequal(self):
        self.assertNotEqual(self.result, self.incorrect)

    def test_type(self):
        self.assertEqual(type(self.result), type(self.type_test))

    def test_concat(self):
        self.assertEqual(self.hello_there, self.concatenated)

    def test_not_concat(self):
        self.assertNotEqual(self.concatenated, self.general_kenobi)


if __name__ == '__main__':
    unittest.main()